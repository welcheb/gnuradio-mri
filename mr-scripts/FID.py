#!/usr/bin/env python
__author__ = "Chris Hasselwander"
__copyright__ = "Copyright 2016"
__credits__ = ["Chris Hasselwander", "Will Grissom"]
__version__ = "1.0.1"
__status__ = "Production"
from pylab import *
import pulses

from lorentzian import lorentz,fit_lorentz
import time
import threading
import pickle
from tabulate import tabulate

class dat(object):
    def __init__(self):
        self.rawdata = ()
        self.kdata = ()

class pulse_params(object):
    def __init__(self):
        # Set some default parameters
        
        f = open('FID_config.txt', 'r')
        foo = {}
        for line in f:
            k,v,bar = [q.strip() for q in line.split(',')]
            if k == 'param_file':
                self.filename = v
            elif k == 'leader_ID':
                self.serial_no = "serial = " + v
            else:   
                foo[k] = float(v)

        f.close()
        
        self.__dict__.update(foo)
        self.power = .2
        self.fudge = 40e-6
        self.samp_rate = tb.samp_rate
        
        
    def set_pulses(self):
    
        # Excitation Pulse
        self.ex = self.power*pulses.hard_pulse(self.p90,self.samp_rate)
        tb.ex_pulse.set_data(self.ex)
        tb.set_ex_delay(int(self.fudge*self.samp_rate))
        
       
        # Readout Window
        self.read = pulses.hard_pulse(self.readout_length,self.samp_rate)
        tb.readwin.set_data(self.read)
        delay = self.fudge+self.p90+self.dead_time
        tb.set_readout_delay(int((delay)*self.samp_rate))

    def set_TR(self,TR):
        # update everything dependent on TR
        self.TR = TR
        tb.set_TR(TR)
        print "Set TR to: " + repr(self.TR)
            
    def set_dead_time(self,dead_time):
        self.dead_time = dead_time
        self.set_pulses()
        print "Set dead_time to: " + repr(self.dead_time)
        
    
    def set_p90(self,p90):
        # update everything dependent on p90
        self.p90 = p90
        self.set_pulses()
        print "Set p90 to: " +repr(self.p90)
        
    def set_readout_length(self,readout_length):
        self.readout_length = readout_length
        self.set_pulses()
        print "Set readout_length to: " + repr(self.readout_length)
        
    def set_nav(self,nav):
        self.nav = nav
        print "Set nav to: " + repr(self.nav)
    
    def param_table(self):
        dtype = [('param','|S25'),('value',float),('units','|S25')]
        tab = np.array([],dtype=dtype)
        f = open('FID_config.txt','r')
        
        for line in f:
            a,b,c = [x.strip() for x in line.split(',')]
            s = 'params.' + a
            b = eval(s)
            values = [(a,float(b),c.lstrip())]
            
            foo = np.asarray(values,dtype=dtype)
            tab = np.append(tab,foo)
            
        param_table = tabulate(tab,headers=["Param Name","Current Value","Units"])
        print param_table
      
def run():
    tb.set_RUN(0)
    params.data = np.zeros([1,params.readout_length*params.samp_rate],float)                      # initialize data
    tb.signal_out.reset()
    
    scan_time = params.nav*params.TR
        
    def read():
        tb.set_RUN(0)
        data.rawdata = np.asarray(tb.signal_out.data())
        data.kdata   = np.sum(np.reshape(data.rawdata,[params.nav,np.size(data.rawdata)/params.nav]),0)
        tb.set_RUN(1)
        print "****SCAN COMPLETE****"
            
        
    th = threading.Timer(scan_time,read,[])
    th.daemon=True
    tb.set_RUN(1)
    th.start()
        
def calibrate_offset(nav):
    tb.set_RUN(0)
    params.data = np.zeros([1,params.readout_length*params.samp_rate],float)                      # initialize data
    tb.signal_out.reset()
    
    scan_time = nav*params.TR
        
    def read():
        tb.set_RUN(0)
        raw = np.asarray(tb.signal_out.data())
        params.data   = np.sum(np.reshape(raw,[nav,np.size(raw)/nav]),0)
        tb.set_RUN(1)
            
        
    th = threading.Timer(scan_time,read,[])
    th.daemon=True
    tb.set_RUN(1)
    th.start()
    th.join()
    
    #process
    sz = int(params.samp_rate)
    pad = np.zeros([1,(sz-size(params.data))],complex)
    fitdata = np.append(params.data,pad)
    # run fft
    spectrum = np.abs(np.fft.fftshift(np.fft.fft(np.fft.fftshift(fitdata))))
    # set up frequency scale
    f = np.arange(float(-sz/2),float(sz/2),float(sz)/float(fitdata.size))
    fit = fit_lorentz(f,spectrum) # fit data to Lorentzian
    o1 = float(f[np.argmax(fit)]) # find frequency at maximum point
    
    # plot results
    plt.clf()
    plt.plot(f,np.abs(spectrum),f,fit,'r')
    plt.plot(o1,np.abs(fit[np.argmax(fit)]),'ro')
    plt.vlines(o1,0,np.abs(spectrum[np.argmax(fit)]),'r')
    s = 'Offset is ' + repr(o1)
    plt.title(s)
    plt.show();plt.draw()
    tb.set_offset(tb.offset+o1)
    params.offset = params.offset+o1    # update offset
    
    
    
def calibrate_power(nav):    
    tb.set_RUN(0)
    start = 0
    step = .1                 
    x = np.asarray([])              
    y = np.asarray([])              
    
    e = 100
    lc = 0
    s = 0
    amp = start
    while step>.005 :
        lc += 1
        x = np.append(x,amp)
        params.power = amp
        params.set_pulses()
        time.sleep(params.TR)
        foo = np.zeros([int(params.readout_length*params.samp_rate),1],float)
        for jj in range(1,nav+1):           # step through averages
            tb.set_RUN(1)
            time.sleep(params.TR)
            tb.set_RUN(0)
            foo = foo+(np.asarray(tb.signal_out.data()[-int(params.readout_length*params.samp_rate):]))              # format data
                    
        y = np.append(y,np.sum(np.abs(foo)))    # y axis is sum of signal
        
        if lc == 1:
            amp = amp + step
        else:
            s = np.append(s,sign(y[-1]-y[-2]))
            d = np.sign(y[-1]-y[-2])*np.sign(x[-1]-x[-2])
            if s[-1]*s[-2]==-1: 
                step = step/2
            amp = amp + step*d
            
            e = y[-1] - y[-2]
            
            if abs(e) < np.max(y)*.001:
                break
           
        # plot results
        plt.clf()
        plt.title('Searching for Optimal Power...')
        plt.xlabel('SDR output amp (V)');plt.ylabel('Amp (a.u.)')
        plt.plot(x,y,'.')
        plt.show()
        plt.draw()
    
    params.power = (x[argmax(y)])
    plt.plot(x[argmax(y)],argmax(y),'rx')
    params.set_pulses()
    tb.set_RUN(1)

def save_calib():
    # set relevant calibration info in dictionary
    auc = params.p90*params.power/np.sqrt(2.)
    calib_info = {"offset":params.offset, "auc":auc} 
    output = open('cal.pkl','wb')
    pickle.dump(calib_info,output)  # save dictionary to file cal.pkl
    output.close()

def show_pulses():
    T = 2*params.fudge+params.p90+params.dead_time+params.readout_length
    length = int(round(params.samp_rate*T))
    step = T/length
    t = np.asarray(range(length))*step*1000 # in milliseconds

    # RF    
    rf = np.ravel(np.zeros([length,1],complex))
    p90_start = (params.samp_rate*params.fudge)
    rf[p90_start:p90_start+params.ex.size] = params.ex
    
    # Readout Window
    readout_win = np.ravel(np.zeros([length,1],float))
    readoutwin_start = int(round(params.samp_rate*(params.fudge+params.p90+params.dead_time)))
    readout_win[readoutwin_start:readoutwin_start+params.read.size] = params.read

    # plot    
    plt.clf()
    RF, = plt.plot(t,np.real(rf),'b')
    Readout_win, = plt.plot(t,readout_win,'--k')
    
    plt.ylim([-.1,1.1])
    plt.xlabel("Time (ms)")
    plt.ylabel("Amplitude (V)")
    plt.title("Pulse Sequence")
    legend([RF,Readout_win],['RF','Readout Window'])
    draw()

def end():
    tb.stop()       # stop flowgraph
    tb.close()      # close GUI window        
        
if __name__ == '__main__':

    execfile("fid_grc_1.py")  # run flowgraph (named tb)
    
    try:
        params.import_params(params.filename)
    except:
        print "No file named %s" % params.filename

    data = dat()
    params = pulse_params()
    tb.set_TR(params.TR)
    tb.set_offset(params.offset)
	tb.qtgui_time_sink_x_0.set_nsamps(int(params.readout_length*params.samp_rate))
    params.set_pulses()          # setup pulses

    time.sleep(2)
